# CHANGELOG

## 2.1.0

* feat: add `private_zone_domain_names` and `public_zone_domain_names` outputs
* chore: bump pre-commit hooks
* chore: replace deprecated `git` to `https` protocol for hooks URL

## 2.0.0

* refactor: wild-beaverize the module
* refactor: (BREAKING) modernize the module: heavily refactor the way zones are handled
* refactor: (BREAKING) modernize the module: heavily refactor the way records are handled
* refactor: (BREAKING) updates minimum requirements to terraform 1.0.5
* refactor: adds variables validations
* refactor: moves resolvers and forward rules to its own file
* refactor: changes forward rules and resolvers outputs to be null of non are created
* test: updates the examples in three: defaults, external and disable
* test: removes alternative examples: moved to default
* test: removes README in examples to ease the maintenance
* maintenance: updates pre-commit
* maintenance: updates .gitignore with latest practices

## 1.0.1

* Fix: pre-commit issues

## 1.0.0

* BREAKING : `rule_forward_resolver_target_ips` is now (object(list(string))) i.e `{"0" => ["127.0.0.1"]}`
* Convert from terraform 0.11 to 0.12
