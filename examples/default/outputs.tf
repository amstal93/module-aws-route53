######
# Default simple example (private zone)
######

output "default_private_delegation_set_ids" {
  value = module.default_private.delegation_set_ids
}

output "default_private_delegation_set_arns" {
  value = module.default_private.delegation_set_arns
}

output "default_private_delegation_set_name_servers" {
  value = module.default_private.delegation_set_name_servers
}

output "default_private_public_zone_arns" {
  value = module.default_private.public_zone_arns
}

output "default_private_public_zone_ids" {
  value = module.default_private.public_zone_ids
}

output "default_private_public_zone_domain_names" {
  value = module.default_private.public_zone_domain_names
}

output "default_private_public_zone_name_servers" {
  value = module.default_private.public_zone_name_servers
}

output "default_private_private_zone_arns" {
  value = module.default_private.private_zone_arns
}

output "default_private_private_zone_ids" {
  value = module.default_private.private_zone_ids
}

output "default_private_private_zone_name_servers" {
  value = module.default_private.private_zone_name_servers
}

output "default_private_private_zone_domain_names" {
  value = module.default_private.private_zone_domain_names
}

output "default_private_dnssec_ksk_ids" {
  value = module.default_private.dnssec_ksk_ids
}

output "default_private_dnssec_ksk_records" {
  value = module.default_private.dnssec_ksk_records
}

output "default_private_dnssec_ksk_ds_records" {
  value = module.default_private.dnssec_ksk_ds_records
}

output "default_private_dnssec_ksk_public_keys" {
  value = module.default_private.dnssec_ksk_public_keys
}

output "default_private_dnssec_ksk_digest_values" {
  value = module.default_private.dnssec_ksk_digest_values
}

output "default_private_dnssec_ksk_digest_algorithm_types" {
  value = module.default_private.dnssec_ksk_digest_algorithm_types
}

output "default_private_dnssec_ksk_digest_algorithm_mnemonics" {
  value = module.default_private.dnssec_ksk_digest_algorithm_mnemonics
}

output "default_private_dnssec_ksk_signing_algorithm_type" {
  value = module.default_private.dnssec_ksk_signing_algorithm_type
}

output "default_private_dnssec_kms_key_id" {
  value = module.default_private.dnssec_kms_key_id
}

output "default_private_dnssec_kms_key_arn" {
  value = module.default_private.dnssec_kms_key_arn
}

output "default_private_dnssec_kms_key_alias_arn" {
  value = module.default_private.dnssec_kms_key_alias_arn
}

output "default_private_resolver_inbound_security_group_ids" {
  value = module.default_private.resolver_inbound_security_group_ids
}

output "default_private_resolver_inbound_ids" {
  value = module.default_private.resolver_inbound_ids
}

output "default_private_resolver_inbound_arns" {
  value = module.default_private.resolver_inbound_arns
}

output "default_private_resolver_inbound_host_vpc_ids" {
  value = module.default_private.resolver_inbound_host_vpc_ids
}

output "default_private_resolver_outbound_security_group_id" {
  value = module.default_private.resolver_outbound_security_group_id
}

output "default_private_resolver_outbound_ids" {
  value = module.default_private.resolver_outbound_ids
}

output "default_private_resolver_outbound_arns" {
  value = module.default_private.resolver_outbound_arns
}

output "default_private_resolver_outbound_host_vpc_ids" {
  value = module.default_private.resolver_outbound_host_vpc_ids
}

output "default_private_rule_forward_ids" {
  value = module.default_private.rule_forward_ids
}

output "default_private_rule_forward_arns" {
  value = module.default_private.rule_forward_arns
}

output "default_private_rule_forward_owner_ids" {
  value = module.default_private.rule_forward_owner_ids
}

output "default_private_rule_forward_share_statuses" {
  value = module.default_private.rule_forward_share_statuses
}

output "default_private_rule_association_forward_id" {
  value = module.default_private.rule_association_forward_id
}

output "default_private_rule_forward_share_ids" {
  value = module.default_private.rule_forward_share_ids
}

output "default_private_rule_forward_share_arns" {
  value = module.default_private.rule_forward_share_arns
}

output "default_private_resource_association_forward_id" {
  value = module.default_private.resource_association_forward_id
}

output "default_private_principal_association_forward_id" {
  value = module.default_private.principal_association_forward_id
}

output "default_private_record_names" {
  value = module.default_private.record_names
}

output "default_private_record_fqdns" {
  value = module.default_private.record_fqdns
}

######
# Default simple example (public zone)
######

output "delegation_set_ids" {
  value = module.default_public.delegation_set_ids
}

output "delegation_set_arns" {
  value = module.default_public.delegation_set_arns
}

output "delegation_set_name_servers" {
  value = module.default_public.delegation_set_name_servers
}

output "public_zone_arns" {
  value = module.default_public.public_zone_arns
}

output "public_zone_ids" {
  value = module.default_public.public_zone_ids
}

output "public_public_zone_domain_names" {
  value = module.default_public.public_zone_domain_names
}

output "public_zone_name_servers" {
  value = module.default_public.public_zone_name_servers
}

output "private_zone_arns" {
  value = module.default_public.private_zone_arns
}

output "private_zone_ids" {
  value = module.default_public.private_zone_ids
}

output "public_private_zone_domain_names" {
  value = module.default_public.private_zone_domain_names
}

output "private_zone_name_servers" {
  value = module.default_public.private_zone_name_servers
}

output "dnssec_ksk_ids" {
  value = module.default_public.dnssec_ksk_ids
}

output "dnssec_ksk_records" {
  value = module.default_public.dnssec_ksk_records
}

output "dnssec_ksk_ds_records" {
  value = module.default_public.dnssec_ksk_ds_records
}

output "dnssec_ksk_public_keys" {
  value = module.default_public.dnssec_ksk_public_keys
}

output "dnssec_ksk_digest_values" {
  value = module.default_public.dnssec_ksk_digest_values
}

output "dnssec_ksk_digest_algorithm_types" {
  value = module.default_public.dnssec_ksk_digest_algorithm_types
}

output "dnssec_ksk_digest_algorithm_mnemonics" {
  value = module.default_public.dnssec_ksk_digest_algorithm_mnemonics
}

output "dnssec_ksk_signing_algorithm_type" {
  value = module.default_public.dnssec_ksk_signing_algorithm_type
}

output "dnssec_kms_key_id" {
  value = module.default_public.dnssec_kms_key_id
}

output "dnssec_kms_key_arn" {
  value = module.default_public.dnssec_kms_key_arn
}

output "dnssec_kms_key_alias_arn" {
  value = module.default_public.dnssec_kms_key_alias_arn
}

output "resolver_inbound_security_group_ids" {
  value = module.default_public.resolver_inbound_security_group_ids
}

output "resolver_inbound_ids" {
  value = module.default_public.resolver_inbound_ids
}

output "resolver_inbound_arns" {
  value = module.default_public.resolver_inbound_arns
}

output "resolver_inbound_host_vpc_ids" {
  value = module.default_public.resolver_inbound_host_vpc_ids
}

output "resolver_outbound_security_group_id" {
  value = module.default_public.resolver_outbound_security_group_id
}

output "resolver_outbound_ids" {
  value = module.default_public.resolver_outbound_ids
}

output "resolver_outbound_arns" {
  value = module.default_public.resolver_outbound_arns
}

output "resolver_outbound_host_vpc_ids" {
  value = module.default_public.resolver_outbound_host_vpc_ids
}

output "rule_forward_ids" {
  value = module.default_public.rule_forward_ids
}

output "rule_forward_arns" {
  value = module.default_public.rule_forward_arns
}

output "rule_forward_owner_ids" {
  value = module.default_public.rule_forward_owner_ids
}

output "rule_forward_share_statuses" {
  value = module.default_public.rule_forward_share_statuses
}

output "rule_association_forward_id" {
  value = module.default_public.rule_association_forward_id
}

output "rule_forward_share_ids" {
  value = module.default_public.rule_forward_share_ids
}

output "rule_forward_share_arns" {
  value = module.default_public.rule_forward_share_arns
}

output "resource_association_forward_id" {
  value = module.default_public.resource_association_forward_id
}

output "principal_association_forward_id" {
  value = module.default_public.principal_association_forward_id
}

output "record_names" {
  value = module.default_public.record_names
}

output "record_fqdns" {
  value = module.default_public.record_fqdns
}

#####
# Complete example
#####

output "complete_delegation_set_ids" {
  value = module.complete.delegation_set_ids
}

output "complete_delegation_set_arns" {
  value = module.complete.delegation_set_arns
}

output "complete_delegation_set_name_servers" {
  value = module.complete.delegation_set_name_servers
}

output "complete_public_zone_arns" {
  value = module.complete.public_zone_arns
}

output "complete_public_zone_ids" {
  value = module.complete.public_zone_ids
}

output "complete_public_zone_name_servers" {
  value = module.complete.public_zone_name_servers
}

output "complete_public_zone_domain_names" {
  value = module.complete.public_zone_domain_names
}

output "complete_private_zone_arns" {
  value = module.complete.private_zone_arns
}

output "complete_private_zone_ids" {
  value = module.complete.private_zone_ids
}

output "complete_private_zone_domain_names" {
  value = module.complete.private_zone_domain_names
}

output "complete_private_zone_name_servers" {
  value = module.complete.private_zone_name_servers
}

output "complete_dnssec_ksk_ids" {
  value = module.complete.dnssec_ksk_ids
}

output "complete_dnssec_ksk_records" {
  value = module.complete.dnssec_ksk_records
}

output "complete_dnssec_ksk_ds_records" {
  value = module.complete.dnssec_ksk_ds_records
}

output "complete_dnssec_ksk_public_keys" {
  value = module.complete.dnssec_ksk_public_keys
}

output "complete_dnssec_ksk_digest_values" {
  value = module.complete.dnssec_ksk_digest_values
}

output "complete_dnssec_ksk_digest_algorithm_types" {
  value = module.complete.dnssec_ksk_digest_algorithm_types
}

output "complete_dnssec_ksk_digest_algorithm_mnemonics" {
  value = module.complete.dnssec_ksk_digest_algorithm_mnemonics
}

output "complete_dnssec_ksk_signing_algorithm_type" {
  value = module.complete.dnssec_ksk_signing_algorithm_type
}

output "complete_dnssec_kms_key_id" {
  value = module.complete.dnssec_kms_key_id
}

output "complete_dnssec_kms_key_arn" {
  value = module.complete.dnssec_kms_key_arn
}

output "complete_dnssec_kms_key_alias_arn" {
  value = module.complete.dnssec_kms_key_alias_arn
}

output "complete_resolver_inbound_security_group_ids" {
  value = module.complete.resolver_inbound_security_group_ids
}

output "complete_resolver_inbound_ids" {
  value = module.complete.resolver_inbound_ids
}

output "complete_resolver_inbound_arns" {
  value = module.complete.resolver_inbound_arns
}

output "complete_resolver_inbound_host_vpc_ids" {
  value = module.complete.resolver_inbound_host_vpc_ids
}

output "complete_resolver_outbound_security_group_id" {
  value = module.complete.resolver_outbound_security_group_id
}

output "complete_resolver_outbound_ids" {
  value = module.complete.resolver_outbound_ids
}

output "complete_resolver_outbound_arns" {
  value = module.complete.resolver_outbound_arns
}

output "complete_resolver_outbound_host_vpc_ids" {
  value = module.complete.resolver_outbound_host_vpc_ids
}

output "complete_rule_forward_ids" {
  value = module.complete.rule_forward_ids
}

output "complete_rule_forward_arns" {
  value = module.complete.rule_forward_arns
}

output "complete_rule_forward_owner_ids" {
  value = module.complete.rule_forward_owner_ids
}

output "complete_rule_forward_share_statuses" {
  value = module.complete.rule_forward_share_statuses
}

output "complete_rule_association_forward_id" {
  value = module.complete.rule_association_forward_id
}

output "complete_rule_forward_share_ids" {
  value = module.complete.rule_forward_share_ids
}

output "complete_rule_forward_share_arns" {
  value = module.complete.rule_forward_share_arns
}

output "complete_resource_association_forward_id" {
  value = module.complete.resource_association_forward_id
}

output "complete_principal_association_forward_id" {
  value = module.complete.principal_association_forward_id
}

output "complete_record_names" {
  value = module.complete.record_names
}

output "complete_record_fqdns" {
  value = module.complete.record_fqdns
}
