#####
# Randoms
#####

resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
}

resource "random_integer" "this" {
  min = 4
  max = 254
}

resource "random_integer" "this2" {
  min = 1
  max = 99
}

#####
# Baseline
#####

data "aws_vpc" "default" {
  default = true
}

data "aws_region" "current" {}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id

  filter {
    name   = "availability-zone"
    values = data.aws_availability_zones.available.names
  }

  filter {
    name   = "default-for-az"
    values = ["true"]
  }
}

resource "aws_vpc" "main" {
  cidr_block = format("10.%s.%s.0/24", random_integer.this.result, random_integer.this2.result)
}

resource "aws_vpc" "second" {
  cidr_block       = format("10.%s.%s.0/24", random_integer.this.result, random_integer.this2.result + 1)
  instance_tenancy = "dedicated"
}

resource "aws_route53_zone" "private" {
  name = "${random_string.this.result}.private.tftest.com"

  vpc {
    vpc_id = data.aws_vpc.default.id
  }

  lifecycle {
    ignore_changes = [vpc]
  }
}

resource "aws_route53_zone" "public" {
  name = "${random_string.this.result}.public.tftest.com"
}

resource "aws_elb" "test" {
  name               = "${random_string.this.result}tftest"
  availability_zones = [element(data.aws_availability_zones.available.names, 0)]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }
}

#####
# Default simple example
# Shows how to:
# - create diverse private zones
# - attach VPCs private zones
#####

module "default_private" {
  source = "../../"

  prefix = random_string.this.result

  vpc_id = data.aws_vpc.default.id

  #####
  # Hosted zone
  #####

  private_zones = [
    {
      logical_name       = "vpc-attachments.private.tftest.com"
      name               = "vpc-attachments.private.tftest.com"
      comment            = ".vpc-attachments.private.tftest.com zone for terraform test"
      vpc_attachment_ids = [aws_vpc.main.id, aws_vpc.second.id]
      tags = {
        test = "with multiple VPC attachment"
      }
    },
    {
      logical_name = "simple.private.tftest.com"
      name         = "simple.private.tftest.com"
      comment      = ".simple.private.tftest.com zone for terraform test"
      tags = {
        test = "simple"
      }
    },
    {
      logical_name = "prefix.private.tftest.com"
      name         = ".prefix.private.tftest.com"
      comment      = ".prefix.private.tftest.com zone for terraform test"
      prefix       = random_integer.this.result
      tags = {
        test = "prefix"
      }
    },
  ]

  tags = {
    why = "for terraform test"
  }
}

#####
# Default simple example
# Shows how to:
# - create diverse public zones
# - enable DNSSEC
# - Create DNS key in the module
# - Create a delegation set for a public zone
#####

module "default_public" {
  source = "../../"

  prefix = random_string.this.result

  tags = {
    why = "for terraform test"
  }

  #####
  # Hosted zone
  #####

  public_zones = [
    {
      logical_name = "prefix.public.tftest.com"
      name         = ".prefix.public.tftest.com"
      prefix       = data.aws_region.current.name
      comment      = "${data.aws_region.current.name}.prefix.public.tftest.com zone for terraform test"
      tags = {
        test = "dynamic prefix"
      }
    },
    {
      logical_name   = "delegation-set1.public.tftest.com"
      name           = "delegation-set1.public.tftest.com"
      comment        = ".delegation-set1.public.tftest.com zone for terraform test"
      delegation_set = true
      tags = {
        number = "delegated 1"
      }
    },
    {
      logical_name   = "delegation-set2.public.tftest.com"
      name           = "delegation-set2.public.tftest.com"
      comment        = ".delegation-set2.public.tftest.com zone for terraform test"
      delegation_set = true
      tags = {
        number = "delegated 2"
      }
    },
  ]

  #####
  # DNSSEC
  #####

  dnssec_enable                          = true
  dnssec_ksk_name                        = "${random_string.this.result}ksktftest"
  dnssec_kms_key_alias_name              = "${random_string.this.result}aliastftest"
  dnssec_kms_key_description             = "A DNSSEC key"
  dnssec_kms_key_deletion_window_in_days = 8
  dnssec_kms_key_tags = {
    test = "create DNSSEC key for public hosted zones"
  }
}

#####
# Complete example
# Shows how to:
# - existing public/private zones
# - use resolvers
# - use forward rules
# - adds records and aliases to the hosted zones
#####

module "complete" {
  source = "../../"

  prefix = random_string.this.result

  vpc_id = data.aws_vpc.default.id

  existing_zones = [
    {
      logical_name = "private.tftest.com"
      zone_id      = aws_route53_zone.private.zone_id
    },
    {
      logical_name = "public.tftest.com"
      zone_id      = aws_route53_zone.public.zone_id
    },
  ]

  private_zones = [
    {
      logical_name = "resolver.private.tftest.com"
      name         = ".resolver.private.tftest.com"
      comment      = "resolver.private.tftest.com zone for terraform test"
      tags = {
        test = "resolver and records"
      }
    },
  ]

  public_zones = [
    {
      logical_name = "dnssec-disabled.public.tftest.com"
      name         = ".dnssec-disabled.public.tftest.com"
      comment      = ".dnssec-disabled.public.tftest.com zone for terraform test"
      dnssec       = false
      tags = {
        test = "DNSSEC disabled and records"
      }
    },
    {
      logical_name = "dnssec-enabled.public.tftest.com"
      name         = ".dnssec-enabled.public.tftest.com"
      comment      = ".dnssec-enabled.public.tftest.com zone for terraform test"
      dnssec       = true
      tags = {
        test = "DNSSEC enabled and records"
      }
    },
  ]

  #####
  # DNSSEC
  #####

  dnssec_enable                          = true
  dnssec_ksk_name                        = "${random_string.this.result}kskcompletetftest"
  dnssec_kms_key_alias_name              = "${random_string.this.result}aliascompletetftest"
  dnssec_kms_key_description             = "A DNSSEC key for complete test"
  dnssec_kms_key_deletion_window_in_days = 8
  dnssec_kms_key_tags = {
    test = "create DNSSEC key for public hosted zones"
  }

  #####
  # Resolvers inbound
  #####

  resolver_tags = {
    Name = "${random_string.this.result}tftest"
  }
  resolver_inbound_count = 1
  resolver_inbound_names = ["${random_string.this.result}inResolver"]
  resolver_inbound_ip_addresses = {
    "0" = [
      format("172.31.0.%s", random_integer.this.result),
      format("172.31.16.%s", random_integer.this.result),
    ]
  }
  resolver_inbound_subnet_ids = {
    "0" = tolist(data.aws_subnet_ids.all.ids)
  }
  resolver_inbound_security_group_name                  = "${random_string.this.result}inResolver"
  resolver_inbound_security_group_ingress_allowed_cidrs = ["192.168.0.0/16", "10.0.0.0/8"]
  resolver_inbound_security_group_egress_allowed_cidrs  = ["10.0.0.0/8"]

  #####
  # Resolvers outbound
  #####

  resolver_outbound_count = 1
  resolver_outbound_names = ["${random_string.this.result}outResolver"]
  resolver_outbound_ip_addresses = {
    "0" = [
      format("172.31.1.%s", random_integer.this.result),
      format("172.31.17.%s", random_integer.this.result),
    ]
  }
  resolver_outbound_subnet_ids = {
    "0" = tolist(data.aws_subnet_ids.all.ids)
  }
  resolver_outbound_security_group_name                  = "${random_string.this.result}outResolver"
  resolver_outbound_security_group_ingress_allowed_cidrs = ["192.168.0.0/16", "10.0.0.0/8"]
  resolver_outbound_security_group_egress_allowed_cidrs  = ["192.168.0.0/16", "10.0.0.0/8"]

  #####
  # Forward rules
  #####

  rule_forward_share_indexes = []
  rule_forward_count         = 1
  rule_forward_domain_names  = ["${random_string.this.result}.tftest-rule-example.com"]
  rule_forward_names         = ["${random_string.this.result}ruleForward"]
  rule_forward_resolver_target_ips = {
    "0" = [
      "123.45.67.5",
      "123.45.68.5"
    ]
  }
  rule_forward_tags = {
    Name = "${random_string.this.result}tftest"
  }
  rule_forward_vpc_attachment_count = 3
  rule_forward_vpc_attachment_ids   = [data.aws_vpc.default.id, aws_vpc.main.id, aws_vpc.second.id]

  #####
  # Records
  #####

  records = [
    {
      zone_logical_name = "resolver.private.tftest.com"
      name              = "record1"
      logical_name      = "record1"
      type              = "A"
      ttl               = 10
      records           = [format("172.31.2.%s", random_integer.this.result)]
    },
    {
      zone_logical_name = "resolver.private.tftest.com"
      name              = "r1.prefix"
      logical_name      = "prefix"
      type              = "CNAME"
      ttl               = 30
      records           = ["${random_string.this.result}record3.${random_string.this.result}.private.tftest.com"]
    },
    {
      zone_logical_name = "private.tftest.com"
      logical_name      = "record3"
      name              = "record3"
      type              = "A"
      ttl               = 100
      records           = [format("1.2.3.%s", random_integer.this.result)]
    },
    {
      zone_logical_name = "public.tftest.com"
      logical_name      = "record4"
      name              = "record4"
      type              = "A"
      ttl               = 3600
      records           = [format("1.2.3.%s", random_integer.this.result)]
    },
  ]

  alias_records = [
    {
      zone_logical_name = "public.tftest.com"
      logical_name      = "alias"
      name              = "alias"
      type              = "A"
      alias_name        = aws_elb.test.dns_name
      alias_zone_id     = aws_elb.test.zone_id
    }
  ]
}
