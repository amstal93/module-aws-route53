# Terraform Route53 module

This module is not to be deployed directly.

## Limitations

- Records cannot be added in a private and public zone that have the exact same name.
If it is the case, the record will be added to the first matching zone ID found in this list: 1. existing zone 2. public zone created by the module 3. private zone created by the module.
To work around this issue, you can call the module multiple times.
- All resolvers of the same type shares the same security group rules.
- All resolvers must have an exact number of 2 IPs. This cannot be worked around in Terraform 0.11.
- There is no way to auto accept resource shares with the aws provider 2.18.
That’s why the resource shares created with this module must be accepted manually on receiving accounts.
- AWS does not offer a way to auto-accept zones associations between account.
In the case zones must be shared between VPCs of different account, this should be done manually until this is solved: https://github.com/terraform-providers/terraform-provider-aws/issues/617.
See this https://docs.aws.amazon.com/cli/latest/reference/route53/create-vpc-association-authorization.html for manual handling.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.5 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.15 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.15 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_kms_alias.this_dnssec](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.this_dnssec](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_ram_principal_association.this_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_principal_association) | resource |
| [aws_ram_resource_association.this_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_resource_association) | resource |
| [aws_ram_resource_share.this_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ram_resource_share) | resource |
| [aws_route53_delegation_set.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_delegation_set) | resource |
| [aws_route53_hosted_zone_dnssec.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_hosted_zone_dnssec) | resource |
| [aws_route53_key_signing_key.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_key_signing_key) | resource |
| [aws_route53_record.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_record.this_alias](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_resolver_endpoint.this_inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_endpoint) | resource |
| [aws_route53_resolver_endpoint.this_outbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_endpoint) | resource |
| [aws_route53_resolver_rule.this_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_rule) | resource |
| [aws_route53_resolver_rule_association.this_forward](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_resolver_rule_association) | resource |
| [aws_route53_zone.private](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [aws_route53_zone.public](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [aws_route53_zone_association.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone_association) | resource |
| [aws_security_group.this_inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group.this_outbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.this_inbound_53_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_53_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_853_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_853_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_out_53_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_out_53_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_out_853_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_inbound_out_853_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_53_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_53_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_853_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_853_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_out_53_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_out_53_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_out_853_tcp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.this_outbound_out_853_udp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.this_dnssec](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_subnet.this_inbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |
| [aws_subnet.this_outbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnet) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_alias_records"></a> [alias\_records](#input\_alias\_records) | List of records to create. Each record will be linked to the `zone_name` zone, looking up in the public zones first, then private zones.<br>  * zone\_logical\_name (required, string):          Logical name of the zone where to create the record. Careful, this value cannot be dynamic, it's the `logical_name` chosen in `var.public_zones` or `var.private_zones`.<br>  * logical\_name (required, string):               Logical name of the record. This value does not appear in the record itself. Careful, this value cannot be dynamic.<br>  * name (optional, string):                       Record name (subdomain) to be added to the zone.<br>  * type (required, string):                       The record type. Valid values are `A`, `AAAA`, `CAA`, `CNAME`, `DS`, `MX`, `NAPTR`, `NS`, `PTR`, `SOA`, `SPF`, `SRV` and `TXT`.<br>  * alias\_name (required, string):                 DNS domain names for a CloudFront distribution, S3 bucket, ELB, or another resource record for the alias records to create.<br>  * alias\_zone\_id (required, string):              Zone ID of a CloudFront distribution, S3 bucket, ELB, or another resource for the alias records to create.<br>  * alias\_evaluate\_target\_health (required, bool): Zone ID of a CloudFront distribution, S3 bucket, ELB, or another resource for the alias records to create. | <pre>list(object({<br>    zone_logical_name            = string<br>    name                         = string<br>    logical_name                 = string<br>    type                         = string<br>    alias_name                   = string<br>    alias_zone_id                = string<br>    alias_evaluate_target_health = optional(bool)<br>  }))</pre> | `[]` | no |
| <a name="input_dnssec_enable"></a> [dnssec\_enable](#input\_dnssec\_enable) | Whether or not to enable DNSSEC for `var.public_zones` or `var.existing_zones` that have the `dnssec` toggle to `true`. | `bool` | `true` | no |
| <a name="input_dnssec_external_key_enabled"></a> [dnssec\_external\_key\_enabled](#input\_dnssec\_external\_key\_enabled) | Whether or not an external KMS is passed. Should be used in combination with `var.dnssec_external_kms_key_arn`. Terraform 1.0.X and less cannot infer `var.dnssec_external_kms_key_arn` result if not empty, that’s why it needs this variable. | `bool` | `false` | no |
| <a name="input_dnssec_external_kms_key_arn"></a> [dnssec\_external\_kms\_key\_arn](#input\_dnssec\_external\_kms\_key\_arn) | ARN of an existing KMS key (ARN of the KMS key alias) to be used for DNSSEC. The same key will be used for all the DNSSEC-enabled public zones. | `any` | `null` | no |
| <a name="input_dnssec_kms_key_alias_name"></a> [dnssec\_kms\_key\_alias\_name](#input\_dnssec\_kms\_key\_alias\_name) | Name of the KMS key alias to create for DNSSEC. The same key will be used for all the DNSSEC-enabled public zones. Ignored if `var.dnssec_external_enabled` is `true`. | `string` | `"default-alias"` | no |
| <a name="input_dnssec_kms_key_deletion_window_in_days"></a> [dnssec\_kms\_key\_deletion\_window\_in\_days](#input\_dnssec\_kms\_key\_deletion\_window\_in\_days) | Deletion window of the KMS key to create for DNSSEC. The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. Ignored if `var.dnssec_external_enabled` is `true`. | `number` | `7` | no |
| <a name="input_dnssec_kms_key_description"></a> [dnssec\_kms\_key\_description](#input\_dnssec\_kms\_key\_description) | Description of the KMS key to create for DNSSEC. Ignored if `var.dnssec_external_enabled` is `true`. | `string` | `"For DNSSEC signing."` | no |
| <a name="input_dnssec_kms_key_tags"></a> [dnssec\_kms\_key\_tags](#input\_dnssec\_kms\_key\_tags) | Tags for the KMS key to create for DNSSEC. Ignored if `var.dnssec_external_enabled` is `true`. Will be merged with `var.tags`. | `map(string)` | `{}` | no |
| <a name="input_dnssec_ksk_name"></a> [dnssec\_ksk\_name](#input\_dnssec\_ksk\_name) | Name of the KSK keys to create for DNSSEC. One KSK per DNSSEC-enabled zone will be created. They can share the same name as they will share the same KMS key. | `string` | `""` | no |
| <a name="input_existing_zones"></a> [existing\_zones](#input\_existing\_zones) | List of route53 hosted zones that already exists outside of this module:<br>  * logical\_name (required, string):             Logical name of the private/public zone. Careful, as logical names will be state file keys, they cannot be dynamic.<br>  * zone\_id (string):                            IDs of an existing hosted zone, public or private, to attach VPC and records.<br>  * vpc\_attachment\_ids (optional, list(string)): IDs of the VPC to be attached to the private hosted zones only. If this is specify with a public zone ID, it will throw an error.<br>  * dnssec (optional, bool):                     Whether or not to enable DNSSEC on this zone. Note this will only work for public zones. Makes sure `var.dnssec_enable` is enabled if you set this value to `true`. Defaults to `false`. | <pre>list(object({<br>    logical_name       = string<br>    zone_id            = string<br>    vpc_attachment_ids = optional(list(string))<br>    dnssec             = optional(bool)<br>  }))</pre> | `[]` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | Prefix to be added to all this module resource's names. Mostly useful for testing purposes. | `string` | `""` | no |
| <a name="input_private_zones"></a> [private\_zones](#input\_private\_zones) | List of route53 hosted private zones to create. `name` as keys:<br>  * logical\_name (required, string):             Logical name of the private zone. Careful, as logical names will be state file keys, they cannot be dynamic.<br>  * name (required, string):                     Name of the private zone (domain name).<br>  * comment (required, string):                  Comment (description) of the private zone.<br>  * tags (required, map(string)):                Specific tags for the zone to be merged with the default tags of the module `var.tags`.<br>  * prefix (optional, string):                   Prefix of the name.<br>  * vpc\_attachment\_ids (optional, list(string)): IDs of the VPC to be attached to the private hosted zone. This should not contain the `var.vpc_id` as it will be attached automatically. | <pre>list(object({<br>    logical_name       = string<br>    name               = string<br>    comment            = string<br>    prefix             = optional(string)<br>    vpc_attachment_ids = optional(list(string))<br>    tags               = optional(map(string))<br>  }))</pre> | `[]` | no |
| <a name="input_public_zones"></a> [public\_zones](#input\_public\_zones) | List of route53 hosted public zones to create:<br>  * logical\_name (required, string):         Name of the public zone (domain name). Careful, as names will be state file keys, they cannot be dynamic.<br>  * name (required, string):         Name of the public zone (domain name). Careful, as names will be state file keys, they cannot be dynamic.<br>  * comment (required, string):      Comment (description) of the public zone.<br>  * prefix (optional, string):       Prefix of the name. This value can by dynamic, contrary to the name.<br>  * delegation\_set (optional, bool): Whether or not to create a delegation set with the public zone, to be able to reuse the NS servers. Defaults to `false`.<br>  * dnssec (optional, bool):         Whether or not to enable DNSSEC on this zone. Note this will only work for public zones. Makes sure `var.dnssec_enable` is enabled if you set this value to `true`. Defaults to `true`.<br>  * tags (optional, map(string)):    Specific tags for the zone to be merged with the default tags of the module `var.tags`. | <pre>list(object({<br>    logical_name   = string<br>    name           = string<br>    comment        = string<br>    prefix         = optional(string)<br>    delegation_set = optional(bool)<br>    dnssec         = optional(bool)<br>    tags           = optional(map(string))<br>  }))</pre> | `[]` | no |
| <a name="input_records"></a> [records](#input\_records) | List of records to create. Each record will be linked to the `zone_name` zone, looking up in the public zones first, then private zones.<br>  * zone\_logical\_name (required, string): Logical name of the zone where to create the record. Careful, this value cannot be dynamic, it's the `logical_name` chosen in `var.public_zones` or `var.private_zones`.<br>  * logical\_name (required, string):      Logical name of the record. This value does not appear in the record itself. Careful, this value cannot be dynamic.<br>  * name (optional, string):              Record name (subdomain) to be added to the zone.<br>  * type (required, string):              The record type. Valid values are `A`, `AAAA`, `CAA`, `CNAME`, `DS`, `MX`, `NAPTR`, `NS`, `PTR`, `SOA`, `SPF`, `SRV` and `TXT`.<br>  * ttl (required, number):               The time-to-live of the record in seconds.<br>  * records (required, set(string)):      The record value. To specify a single record value longer than 255 characters such as a TXT record for DKIM, add `\"\"` inside the Terraform configuration string (e.g., \"first255characters\"\"morecharacters"`).<br>` | <pre>list(object({<br>    zone_logical_name = string<br>    name              = string<br>    logical_name      = string<br>    type              = string<br>    ttl               = number<br>    records           = set(string)<br>  }))</pre> | `[]` | no |
| <a name="input_resolver_inbound_count"></a> [resolver\_inbound\_count](#input\_resolver\_inbound\_count) | How many INBOUND resolvers to be created in the module. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| <a name="input_resolver_inbound_ip_addresses"></a> [resolver\_inbound\_ip\_addresses](#input\_resolver\_inbound\_ip\_addresses) | Object of lists containing the IP addresses corresponding to the subnet IDs for the INBOUND resolvers to be created in the module. Look at examples for correct usage. | `map` | `{}` | no |
| <a name="input_resolver_inbound_names"></a> [resolver\_inbound\_names](#input\_resolver\_inbound\_names) | Names of the INBOUND resolvers to be created in the module. | `list` | `[]` | no |
| <a name="input_resolver_inbound_security_group_egress_allowed_cidrs"></a> [resolver\_inbound\_security\_group\_egress\_allowed\_cidrs](#input\_resolver\_inbound\_security\_group\_egress\_allowed\_cidrs) | CIDRs allowed to perform DNS request to the INBOUND resolvers, egress rules. | `list` | <pre>[<br>  "10.0.0.0/8"<br>]</pre> | no |
| <a name="input_resolver_inbound_security_group_ingress_allowed_cidrs"></a> [resolver\_inbound\_security\_group\_ingress\_allowed\_cidrs](#input\_resolver\_inbound\_security\_group\_ingress\_allowed\_cidrs) | CIDRs allowed to perform DNS request to the INBOUND resolvers, ingress rules. | `list` | <pre>[<br>  "10.0.0.0/8"<br>]</pre> | no |
| <a name="input_resolver_inbound_security_group_name"></a> [resolver\_inbound\_security\_group\_name](#input\_resolver\_inbound\_security\_group\_name) | Name of the security groups shared for INBOUND resolvers. | `string` | `"inbound-resolver"` | no |
| <a name="input_resolver_inbound_subnet_ids"></a> [resolver\_inbound\_subnet\_ids](#input\_resolver\_inbound\_subnet\_ids) | Object of lists containing the subnet IDs corresponding to the IP addresses for the INBOUND resolvers to be created in the module. Look at examples for correct usage. | `map` | `{}` | no |
| <a name="input_resolver_outbound_count"></a> [resolver\_outbound\_count](#input\_resolver\_outbound\_count) | How many OUTBOUND resolvers to be created in the module. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| <a name="input_resolver_outbound_ip_addresses"></a> [resolver\_outbound\_ip\_addresses](#input\_resolver\_outbound\_ip\_addresses) | Object of lists containing the IP addresses corresponding to the subnet IDs for the OUTBOUND resolvers to be created in the module. Look at examples for correct usage. | `map` | `{}` | no |
| <a name="input_resolver_outbound_names"></a> [resolver\_outbound\_names](#input\_resolver\_outbound\_names) | Names of the OUTBOUND resolvers to be created in the module. | `list` | `[]` | no |
| <a name="input_resolver_outbound_security_group_egress_allowed_cidrs"></a> [resolver\_outbound\_security\_group\_egress\_allowed\_cidrs](#input\_resolver\_outbound\_security\_group\_egress\_allowed\_cidrs) | CIDRs allowed to perform DNS request to the OUTBOUND resolvers, egress rules. | `list` | <pre>[<br>  "10.0.0.0/8"<br>]</pre> | no |
| <a name="input_resolver_outbound_security_group_ingress_allowed_cidrs"></a> [resolver\_outbound\_security\_group\_ingress\_allowed\_cidrs](#input\_resolver\_outbound\_security\_group\_ingress\_allowed\_cidrs) | CIDRs allowed to perform DNS request to the OUTBOUND resolvers, ingress rules. | `list` | <pre>[<br>  "10.0.0.0/8"<br>]</pre> | no |
| <a name="input_resolver_outbound_security_group_name"></a> [resolver\_outbound\_security\_group\_name](#input\_resolver\_outbound\_security\_group\_name) | Name of the security groups shared for OUTBOUND resolvers. | `string` | `"outbound-resolver"` | no |
| <a name="input_resolver_outbound_subnet_ids"></a> [resolver\_outbound\_subnet\_ids](#input\_resolver\_outbound\_subnet\_ids) | Object of lists containing the subnet IDs corresponding to the IP addresses for the OUTBOUND resolvers to be created in the module. Look at examples for correct usage. | `map` | `{}` | no |
| <a name="input_resolver_tags"></a> [resolver\_tags](#input\_resolver\_tags) | Tags specific to the resolvers to be created in the module. Will be merged with tags. | `map` | `{}` | no |
| <a name="input_rule_forward_attachment_ids"></a> [rule\_forward\_attachment\_ids](#input\_rule\_forward\_attachment\_ids) | IDs of the forward resolver rules that should be attached to the rule\_forward\_vpc\_attachment\_ids. If not specify, the forward rules created by this module will be used for all the attachments. | `list` | `[]` | no |
| <a name="input_rule_forward_attachment_ids_count"></a> [rule\_forward\_attachment\_ids\_count](#input\_rule\_forward\_attachment\_ids\_count) | How many var.rule\_forward\_attachment\_ids. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| <a name="input_rule_forward_count"></a> [rule\_forward\_count](#input\_rule\_forward\_count) | How many resolvers forward rules to be created in the module. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| <a name="input_rule_forward_domain_names"></a> [rule\_forward\_domain\_names](#input\_rule\_forward\_domain\_names) | Domain names of the resolvers forward rules to be created in the module. DNS queries for these domain names are forwarded to the IP addresses that are specified using target\_ip. | `list` | `[]` | no |
| <a name="input_rule_forward_names"></a> [rule\_forward\_names](#input\_rule\_forward\_names) | Names of the resolvers forward rules to be created in the module. Friendly names that lets you easily find a rule in the Resolver dashboard in the Route 53 console. | `list` | `[]` | no |
| <a name="input_rule_forward_resolver_endpoint_ids"></a> [rule\_forward\_resolver\_endpoint\_ids](#input\_rule\_forward\_resolver\_endpoint\_ids) | IDs of the resolver endpoints to be used for the resolver forward rules. If not specify, the first OUBOUND resolver created by this module will be used for all the rules. | `list` | `[]` | no |
| <a name="input_rule_forward_resolver_target_ips"></a> [rule\_forward\_resolver\_target\_ips](#input\_rule\_forward\_resolver\_target\_ips) | Object of lists of objects containing target IPs for the resolver forward rules. IPs that you want resolvers to forward DNS queries to. Look at examples for correct usage. | `map` | `{}` | no |
| <a name="input_rule_forward_share_indexes"></a> [rule\_forward\_share\_indexes](#input\_rule\_forward\_share\_indexes) | Indexes of the forward rules to be shared with other principals (rule\_forward\_share\_principals). See examples for correct usage. | `list` | `[]` | no |
| <a name="input_rule_forward_share_names"></a> [rule\_forward\_share\_names](#input\_rule\_forward\_share\_names) | Names of the resource shares resolvers for forward rules to be created in the module. | `list` | `[]` | no |
| <a name="input_rule_forward_share_principal_count"></a> [rule\_forward\_share\_principal\_count](#input\_rule\_forward\_share\_principal\_count) | How many accounts must receive the resource shares for forward rules to be created in the module. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| <a name="input_rule_forward_share_principals"></a> [rule\_forward\_share\_principals](#input\_rule\_forward\_share\_principals) | IDs of the accounts that must receive the resource shares for forward rules to be created in the module. | `list` | `[]` | no |
| <a name="input_rule_forward_share_tags"></a> [rule\_forward\_share\_tags](#input\_rule\_forward\_share\_tags) | Tags specific to the resource shares for the forward rules to be created in the module. Will be merged with tags. | `map` | `{}` | no |
| <a name="input_rule_forward_tags"></a> [rule\_forward\_tags](#input\_rule\_forward\_tags) | Tags specific to the resolvers forward rules to be created in the module. Will be merged with tags. | `map` | `{}` | no |
| <a name="input_rule_forward_vpc_attachment_count"></a> [rule\_forward\_vpc\_attachment\_count](#input\_rule\_forward\_vpc\_attachment\_count) | How many resolver forward rule attachments should be created in the module. This value cannot be computed automatically in Terraform 0.11. | `number` | `0` | no |
| <a name="input_rule_forward_vpc_attachment_ids"></a> [rule\_forward\_vpc\_attachment\_ids](#input\_rule\_forward\_vpc\_attachment\_ids) | IDs of the VPC to be attached to the resolver forward rules of this module. | `list` | `[]` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be shared among all resources of this module. | `map(string)` | `{}` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | ID of the VPC where to create resources for this module. Note that public hosted zones are not linked to any VPC, contrary to private zones. | `any` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_delegation_set_arns"></a> [delegation\_set\_arns](#output\_delegation\_set\_arns) | n/a |
| <a name="output_delegation_set_ids"></a> [delegation\_set\_ids](#output\_delegation\_set\_ids) | n/a |
| <a name="output_delegation_set_name_servers"></a> [delegation\_set\_name\_servers](#output\_delegation\_set\_name\_servers) | n/a |
| <a name="output_dnssec_kms_key_alias_arn"></a> [dnssec\_kms\_key\_alias\_arn](#output\_dnssec\_kms\_key\_alias\_arn) | n/a |
| <a name="output_dnssec_kms_key_arn"></a> [dnssec\_kms\_key\_arn](#output\_dnssec\_kms\_key\_arn) | n/a |
| <a name="output_dnssec_kms_key_id"></a> [dnssec\_kms\_key\_id](#output\_dnssec\_kms\_key\_id) | n/a |
| <a name="output_dnssec_ksk_digest_algorithm_mnemonics"></a> [dnssec\_ksk\_digest\_algorithm\_mnemonics](#output\_dnssec\_ksk\_digest\_algorithm\_mnemonics) | n/a |
| <a name="output_dnssec_ksk_digest_algorithm_types"></a> [dnssec\_ksk\_digest\_algorithm\_types](#output\_dnssec\_ksk\_digest\_algorithm\_types) | n/a |
| <a name="output_dnssec_ksk_digest_values"></a> [dnssec\_ksk\_digest\_values](#output\_dnssec\_ksk\_digest\_values) | n/a |
| <a name="output_dnssec_ksk_ds_records"></a> [dnssec\_ksk\_ds\_records](#output\_dnssec\_ksk\_ds\_records) | n/a |
| <a name="output_dnssec_ksk_ids"></a> [dnssec\_ksk\_ids](#output\_dnssec\_ksk\_ids) | n/a |
| <a name="output_dnssec_ksk_public_keys"></a> [dnssec\_ksk\_public\_keys](#output\_dnssec\_ksk\_public\_keys) | n/a |
| <a name="output_dnssec_ksk_records"></a> [dnssec\_ksk\_records](#output\_dnssec\_ksk\_records) | n/a |
| <a name="output_dnssec_ksk_signing_algorithm_type"></a> [dnssec\_ksk\_signing\_algorithm\_type](#output\_dnssec\_ksk\_signing\_algorithm\_type) | n/a |
| <a name="output_principal_association_forward_id"></a> [principal\_association\_forward\_id](#output\_principal\_association\_forward\_id) | n/a |
| <a name="output_private_zone_arns"></a> [private\_zone\_arns](#output\_private\_zone\_arns) | n/a |
| <a name="output_private_zone_domain_names"></a> [private\_zone\_domain\_names](#output\_private\_zone\_domain\_names) | n/a |
| <a name="output_private_zone_ids"></a> [private\_zone\_ids](#output\_private\_zone\_ids) | n/a |
| <a name="output_private_zone_name_servers"></a> [private\_zone\_name\_servers](#output\_private\_zone\_name\_servers) | n/a |
| <a name="output_public_zone_arns"></a> [public\_zone\_arns](#output\_public\_zone\_arns) | n/a |
| <a name="output_public_zone_domain_names"></a> [public\_zone\_domain\_names](#output\_public\_zone\_domain\_names) | n/a |
| <a name="output_public_zone_ids"></a> [public\_zone\_ids](#output\_public\_zone\_ids) | n/a |
| <a name="output_public_zone_name_servers"></a> [public\_zone\_name\_servers](#output\_public\_zone\_name\_servers) | n/a |
| <a name="output_record_fqdns"></a> [record\_fqdns](#output\_record\_fqdns) | n/a |
| <a name="output_record_names"></a> [record\_names](#output\_record\_names) | n/a |
| <a name="output_resolver_inbound_arns"></a> [resolver\_inbound\_arns](#output\_resolver\_inbound\_arns) | n/a |
| <a name="output_resolver_inbound_host_vpc_ids"></a> [resolver\_inbound\_host\_vpc\_ids](#output\_resolver\_inbound\_host\_vpc\_ids) | n/a |
| <a name="output_resolver_inbound_ids"></a> [resolver\_inbound\_ids](#output\_resolver\_inbound\_ids) | n/a |
| <a name="output_resolver_inbound_security_group_ids"></a> [resolver\_inbound\_security\_group\_ids](#output\_resolver\_inbound\_security\_group\_ids) | n/a |
| <a name="output_resolver_outbound_arns"></a> [resolver\_outbound\_arns](#output\_resolver\_outbound\_arns) | n/a |
| <a name="output_resolver_outbound_host_vpc_ids"></a> [resolver\_outbound\_host\_vpc\_ids](#output\_resolver\_outbound\_host\_vpc\_ids) | n/a |
| <a name="output_resolver_outbound_ids"></a> [resolver\_outbound\_ids](#output\_resolver\_outbound\_ids) | n/a |
| <a name="output_resolver_outbound_security_group_id"></a> [resolver\_outbound\_security\_group\_id](#output\_resolver\_outbound\_security\_group\_id) | n/a |
| <a name="output_resource_association_forward_id"></a> [resource\_association\_forward\_id](#output\_resource\_association\_forward\_id) | n/a |
| <a name="output_rule_association_forward_id"></a> [rule\_association\_forward\_id](#output\_rule\_association\_forward\_id) | n/a |
| <a name="output_rule_forward_arns"></a> [rule\_forward\_arns](#output\_rule\_forward\_arns) | n/a |
| <a name="output_rule_forward_ids"></a> [rule\_forward\_ids](#output\_rule\_forward\_ids) | n/a |
| <a name="output_rule_forward_owner_ids"></a> [rule\_forward\_owner\_ids](#output\_rule\_forward\_owner\_ids) | n/a |
| <a name="output_rule_forward_share_arns"></a> [rule\_forward\_share\_arns](#output\_rule\_forward\_share\_arns) | n/a |
| <a name="output_rule_forward_share_ids"></a> [rule\_forward\_share\_ids](#output\_rule\_forward\_share\_ids) | n/a |
| <a name="output_rule_forward_share_statuses"></a> [rule\_forward\_share\_statuses](#output\_rule\_forward\_share\_statuses) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
